package com.example.belajarviewpager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.example.belajarviewpager.adapter.WalkThroughAdapter
import kotlinx.android.synthetic.main.activity_walk_through.*

class WalkThroughActivity : AppCompatActivity() {

    lateinit var wkAdapater: WalkThroughAdapter
    val dots = arrayOfNulls<TextView>(3)
    var currentPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walk_through)

        wkAdapater = WalkThroughAdapter(this)
        vp_walkthrough.adapter = wkAdapater

        dotIndikator(currentPage)

        initAction()
    }

    fun initAction() {
        vp_walkthrough.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                dotIndikator(position)
                currentPage = position
            }

        })
        tv_lanjutkan.setOnClickListener {
            vp_walkthrough.setCurrentItem(currentPage + 1)
        }
        
    }

    fun dotIndikator (p: Int) {
        ll_dots.removeAllViews()

        for (i in 0 ..dots.size-1) {
            dots [i] = TextView(this)
            dots [i]?.textSize = 35f
            dots [i]?.setTextColor(resources.getColor(R.color.grey))

        }

        if (dots.size > 0) {
            dots [p]?.setTextColor(resources.getColor(R.color.colorPrimary))
        }
    }

}